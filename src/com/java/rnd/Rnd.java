package com.java.rnd;

import java.util.Random;

public class Rnd {


    Random rand = new Random();
    int min;
    int max;
    int numberOfTimes;
    public void task1(){
        min=1;
        max=100;
        int number = min + rand.nextInt(max - min + 1);
        System.out.println("Task1 random = "+number);
    }
    public void task2(){
        min=1;
        max=100;
        numberOfTimes=10;
        System.out.println("Task 2 (10 random numbers )");
        for(int i=1;i<=numberOfTimes;i++)
        {
            int number = min + rand.nextInt(max - min + 1);
            System.out.println("Random "+ i+" = "+number);
        }

    }


    public void task3(){
        min=0;
        max=10;
        numberOfTimes=10;
        System.out.println("Task 3 ( 10 random numbers 0 <= x <= 10 )");
        for(int i=1;i<=numberOfTimes;i++)
        {
            int number = min + rand.nextInt(max - min + 1);
            System.out.println("Random "+ i+" = "+number);
        }

    }


    public void task4(){
        min=20;
        max=50;
        numberOfTimes=10;
        System.out.println("Task 4 ( 10 random numbers 20 <= x <= 50 )");
        for(int i=1;i<=numberOfTimes;i++)
        {
            int number = min + rand.nextInt(max - min + 1);
            System.out.println("Random "+ i+" = "+number);
        }

    }


    public void task5(){
        min=-10;
        max=10;
        numberOfTimes=10;
        System.out.println("Task 5 ( 10 random numbers -10 <= x <= 10 )");
        for(int i=1;i<=numberOfTimes;i++)
        {
            int number = min + rand.nextInt(max - min + 1);
            System.out.println("Random "+ i+" = "+number);
        }

    }


    public void task6(){
        min=3;
        max=15;
        numberOfTimes= min + rand.nextInt(max - min + 1);
        min=-10;
        max=35;
        System.out.println("Task 6 ( 3-15 random numbers -10 <= x <= 35 )");
        for(int i=1;i<=numberOfTimes;i++)
        {
            int number = min + rand.nextInt(max - min + 1);
            System.out.println("Random "+ i+" = "+number);
        }

    }
}
